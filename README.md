# Swaysome

An awesome way to manage your workspaces on [sway](https://swaywm.org/).

Join us on [#swaysome:matrix.hya.sk](https://matrix.to/#/%23swaysome:matrix.hya.sk)!


## Description

This binary helps you configure sway to work a bit more like [AwesomeWM](https://awesomewm.org/). This means that
**workspaces** are namespaced in what are called **workspace groups**, and **workspace groups** can be moved around the
differents outputs easily.

For example, with workspace `11` on the first output, and workspace `21` on the second output, triggering the `swaysome focus 1`
shortcut to get workspace `1` would lead you to workspace `11` if your focus is on the first output, and workspace `21`
is the focus is on the second one.

By default, `swaysome init` will create a **workspace group** per active output, but you may create other groups while
working, by either triggering `swaysome focus-group <new-number>` and opening a new window, or sending an existing
window to it first with `swaysome move-to-group <new-number>`.

Here is a common use-case for this:

* `output-1`:
  * **workspace group** 1:
    * workspace `11`: chats
    * workspace `12`: emails
* `output-2`:
  * **workspace group** 2:
    * workspace `21`: IDE for first project
    * workspace `22`: browser for first project
    * workspace `23`: terminals for first project
  * **workspace group** 3:
    * workspace `31`: IDE for second project
    * workspace `32`: browser for second project
    * workspace `33`: terminals for second project

That way, when `output-2` is focused on **workspace group** 2, be it workspace `21` or `22`, the quick `$mod+<number>`
(bound to `swaysome focus <number>`) shortcut won't leave **workspace group** 2, allowing you to open multiple projects
in parallel without the hassle of manually remembering how to namespace them.

In that situation, suppose you plug in a new output, `output-3`, you may then want to focus **workspace group 3**
to send it to `output-3`: this is simply done by typing the shortcuts `$mod+Alt+3` (`swaysome focus-group 3`) then
`$mod+Alt+o` (`swaysome workspace-group-next-output`).


`swaysome` may also work with i3, but this is untested.

`swaysome` should be compatible with [sworkstyle](https://lib.rs/crates/sworkstyle). If this is broken, please report a bug.


## Installation


Arch Linux: Found on the AUR as [swaysome-git](https://aur.archlinux.org/packages/swaysome-git).

Void Linux: `xbps-install swaysome`

If you have Rust installed, then you can just `cargo install swaysome` and you're good to go.

Otherwise, you may grab a [pre-built
binary](https://gitlab.com/hyask/swaysome/-/jobs/artifacts/master/raw/target/release/swaysome?job=build:cargo) from the
CI and put it in your `$PATH`.

**WARNING**: please double-check that your `swaysome` binary is in `sway`'s `$PATH`. Depending on your setup, the
`$PATH` you have in your shell may not be the same as `sway`'s, and if `swaysome` can't be called by `sway`, the
symptoms will only look like non-functional shortcuts.  
If you're in this situation, a quick workaround is to call `swaysome` with its full absolute path from `sway`'s config
to check that everything works before fixing your `$PATH` issue.


## Usage

Copy the `swaysome.conf` file in `~/.config/sway/config.d/swaysome.conf`.

Then append your `sway` configuration with this:
```
include ~/.config/sway/config.d/*.conf
```

On next startup of `sway`, you should end-up with workspaces from `1` to `0`,
prefixed with a screen index, giving you workspace `11` on the first screen, and
workspace `21` on the second one, both accessible with shortcut `$mod+1` when
focused on the right output.

The `init` command simply walks through every screen to initialize a prefixed
workspace. It does it backwards so that you end-up focused on the first screen,
as usual.


## Exhaustive swaysome commands list

Just run `swaysome --help` for the most up to date list of command and documentation.


## Breaking changes

* Starting with 2.0, `next_output` and `prev_output` have been changed to `next-output` and `prev-output`.
